import React from 'react';
import { SafeAreaView, StatusBar } from 'react-native';
import { FloatButton } from '../../components/atoms';
import { Header, BottomNav } from '../../components/molecules';
import { Storyboard } from '../../components/organisms';
import { ChatBox } from '../../components/templates';
import { Search, Menu, Profile, Profile2, Profile3, Profile4, Chating, World, User } from '../../config/images';
import { VOUCHER } from '../../config/navigation';

const Chat = (props) => {
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <StatusBar backgroundColor="#fff" />
            <Header
                title="Obrolan"
                iconRight={[
                    { id: 0, src: Search, style: { height: 19 } },
                    { id: 1, src: Menu, style: { height: 19 } },
                ]}
            />
            <Storyboard
                data={[
                    { id: 1, name: 'Lisa', src: Profile2, hasNewStory: true },
                    { id: 2, name: 'Juan', src: Profile3, hasNewStory: true },
                    { id: 3, name: 'Emily', src: Profile4, hasNewStory: true },
                ]}
            />
            <ChatBox
                tabData={[
                    { id: 0, text: 'Pribadi', isActive: true },
                    { id: 1, text: 'Calon Teman', hasNewData: true },
                    { id: 2, text: 'Call' },
                ]}
                chatData={[
                    { id: 0, src: Profile2, name: 'Adi Pambudi', time: 'Baru', chat: 'Lorem exercitation officia sunt amet Lorem ullamco eu aliqua nulla est fugiat.', unRead: '99+' },
                    { id: 1, src: Profile3, name: 'Bulan', time: 'Kemarin', chat: 'Lorem exercitation officia sunt amet Lorem ullamco eu aliqua nulla est fugiat.', unRead: '' },
                    { id: 2, src: Profile4, name: 'College Buddy', time: '29 Menit', chat: '+6281234567890 sedang mengetik...', unRead: '', typing: true },
                    { id: 3, src: Profile, name: 'Family Member', time: '31 April 2020', chat: 'family.jpg', unRead: '' },
                ]}
            />
            <FloatButton onPress={() => null} />
            <BottomNav
                data={[
                    { id: 0, src: World, isActive: false, onPress: () => props.navigation.navigate(VOUCHER) },
                    { id: 1, src: Chating, isActive: true },
                    { id: 2, src: User, isActive: false },
                ]}
            />
        </SafeAreaView>
    );
};

export default Chat;