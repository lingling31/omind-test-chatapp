import React from 'react';
import { SafeAreaView, StatusBar, ScrollView } from 'react-native';
import { Tutorial, Button } from '../../components/atoms';
import { Header, VoucherCard } from '../../components/molecules';
import { LeftArrow, VoucherKfc, Calendar, Time, QR } from '../../config/images';

const Voucher = (props) => {
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <StatusBar backgroundColor="#fff" />
            <Header
                title="Info Voucher"
                iconLeft={[
                    {
                        id: 0,
                        src: LeftArrow,
                        style: { height: 26, tintColor: '#aaa' },
                        onPress: () => props.navigation.goBack()
                    },
                ]}
            />
            <ScrollView>
                <VoucherCard
                    data={{
                        src: VoucherKfc,
                        title: 'Food Festival 65% KFC',
                        desc: 'Lorem exercitation officia sunt amet Lorem ullamco eu aliqua nulla est fugiat.',
                        label: [
                            { id: 0, src: Calendar, text: 'Berlaku hingga 1 Okt 2020' },
                            { id: 1, src: Time, text: '07:00 - 23:59' },
                        ]
                    }}
                />
                <Tutorial
                    data={{
                        title: 'Cara menggunakan',
                        tutorial: [
                            { id: 0, text: 'Datang ke tempat merchant terkait.' },
                            { id: 1, text: 'Tanyakan kepada mereka dengan ramah untuk menukarkan Voucher.' },
                            { id: 2, text: 'Perlihatkan QR Voucher.' },
                        ]
                    }}
                />
            </ScrollView>
            <Button
                icon={QR}
                text="QR VOUCHER"
                onPress={() => null}
            />
        </SafeAreaView>
    );
};

export default Voucher;