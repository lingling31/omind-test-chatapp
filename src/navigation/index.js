import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Chat, Voucher } from '../pages';
import { CHAT, VOUCHER } from '../config/navigation';

const Stack = createStackNavigator();

const AppNavigation = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator headerMode="none">
                <Stack.Screen name={CHAT} component={Chat} />
                <Stack.Screen name={VOUCHER} component={Voucher} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default AppNavigation;