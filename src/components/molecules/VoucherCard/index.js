import React from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import { VoucherInfo, LabelIcon } from '../../atoms';

const { width, height } = Dimensions.get('window')

const VoucherCard = ({ data }) => {
    const { label } = data
    return (
        <View style={styles.container}>
            <VoucherInfo data={data} />
            <View style={{ paddingVertical: 20 }}>
                {label.map(item => <LabelIcon key={item.id} item={item} />)}
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: width * 0.05,
    },
    text: {
        fontSize: 20,
        fontWeight: 'bold'
    }
})

export default VoucherCard;