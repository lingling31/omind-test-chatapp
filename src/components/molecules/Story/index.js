import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { ProfileImage } from '../../atoms';

const Story = ({ data }) => {
    const { id, name, src, style, hasNewStory } = data
    return (
        <View style={[styles.container, { marginLeft: id === 0 ? 15 : 8 }]}>
            <ProfileImage src={src} style={style} hasNewStory={hasNewStory} />
            <Text style={styles.text}>{name}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        justifyContent: 'space-between',
        width: 70,
        height: 90,
        margin: 10,
    },
    text: {
        alignSelf: 'center',
        fontSize: 15
    }
})

export default Story;