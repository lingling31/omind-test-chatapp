import React from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import { ProfileImage, ChatBody } from '../../atoms';

const { width } = Dimensions.get('window')

const FieldChat = ({ item = {} }) => {
    return (
        <View style={styles.container}>
            <View style={{ flex: 1 }}>
                <ProfileImage src={item.src} />
            </View>
            <View style={{ flex: 4, height: '100%' }}>
                <ChatBody item={item} />
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: width * 0.04,
        paddingVertical: 5
    },
})

export default FieldChat;