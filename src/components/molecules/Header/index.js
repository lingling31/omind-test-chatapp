import React from 'react';
import { View, Dimensions, StyleSheet } from 'react-native';
import { HeaderTitle, Icon } from '../../atoms';

const { width, height } = Dimensions.get('window')

const Header = ({ title, iconLeft = [], iconRight = [] }) => {
    return (
        <View style={styles.container}>
            {iconLeft.length ?
                <IconContainer icon={iconLeft} />
                : null
            }
            <HeaderTitle text={title} />
            {iconRight.length ?
                <IconContainer icon={iconRight} />
                : null
            }
        </View>
    );
};

const IconContainer = ({ icon }) => {
    return (
        <View style={[styles.iconContainer, { flex: icon.length > 1 ? icon.length * 0.8 : 0.9 }]}>
            {icon.map(item => <Icon key={item.id} item={item} />)}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: height * 0.07,
        paddingHorizontal: width * 0.05,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    iconContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    }
})

export default Header;