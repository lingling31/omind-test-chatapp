import React from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import { BottomIcon } from '../../atoms';

const { width, height } = Dimensions.get('window')

const BottomNav = ({ data = [] }) => {
    return (
        <View style={styles.container}>
            {data.map(item => <BottomIcon key={item.id} item={item} />)}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: height * 0.08,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        paddingHorizontal: width * 0.04,
        position: 'absolute',
        bottom: 0,
        borderTopWidth: 1,
        borderColor: '#eee',
        backgroundColor: '#fff'
    },
})

export default BottomNav;