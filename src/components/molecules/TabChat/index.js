import React from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import { ButtonTab } from '../../atoms';

const { width } = Dimensions.get('window')

const TabChat = ({ data = [] }) => {
    return (
        <View style={styles.container}>
            {data.map(item => <ButtonTab key={item.id} item={item} />)}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        paddingVertical: 20,
        paddingRight: width * 0.05,
    },
})

export default TabChat;