import React from 'react';
import { View, Text, StyleSheet, Image, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window')

const VoucherInfo = ({ data }) => {
    const { src, title, desc } = data
    return (
        <View style={styles.container}>
            <Image source={src} style={styles.image} />
            <Text style={styles.text}>{title}</Text>
            <Text style={styles.desc}>{desc}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
    },
    image: {
        height: height * 0.2,
        width: '100%',
        resizeMode: 'contain'
    },
    text: {
        fontSize: 19,
        fontWeight: 'bold',
        marginVertical: 7
    },
    desc: {
        fontSize: 16.5,
        color: '#333'
    }
})

export default VoucherInfo;