import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native'

const LabelIcon = ({ item }) => {
    const { src, text } = item
    return (
        <View style={styles.container}>
            <Image source={src} style={styles.image} />
            <Text style={styles.text}>{text}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        paddingVertical: 4,
        flexDirection: 'row',
        alignItems: 'center'
    },
    image: {
        height: 18,
        resizeMode: 'contain',
        tintColor: '#ACAFB2',
        marginRight: 4
    },
    text: {
        fontSize: 15,
        color: '#888'
    }
})

export default LabelIcon;