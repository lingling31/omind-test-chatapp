import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const HeaderTitle = ({ text }) => {
    return (
        <View style={styles.container}>
            <Text style={styles.text}>{text}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 6
    },
    text: {
        fontSize: 25,
        fontWeight: 'bold'
    }
})

export default HeaderTitle;