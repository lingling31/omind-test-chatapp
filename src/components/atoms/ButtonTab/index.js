import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'

const ButtonTab = ({ item }) => {
    const { text, onPress, isActive, hasNewData } = item
    return (
        <TouchableOpacity
            style={[styles.container, { backgroundColor: isActive ? '#FFF4F4' : null }]}
            onPress={onPress}
        >
            <Text style={[styles.text, { color: isActive ? '#F22B5F' : '#aaa' }]}>{text}</Text>
            {hasNewData && <View style={styles.dot} />}
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    container: {
        paddingVertical: 5,
        paddingHorizontal: 15,
        borderRadius: 25,
        flexDirection: 'row'
    },
    text: {
        fontSize: 19,
        fontWeight: 'bold',
    },
    dot: {
        backgroundColor: '#F22B5F',
        height: 5,
        width: 5,
        borderRadius: 2.5
    }
})

export default ButtonTab;