import React from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window')

const Tutorial = ({ data }) => {
    const { title, tutorial } = data
    return (
        <View style={styles.container}>
            <Text style={styles.text}>{title}</Text>
            {tutorial.map((item, index) => <Text key={item.id} style={styles.tutorial}>{`${index + 1}. ${item.text}`}</Text>)}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: width * 0.05,
    },
    text: {
        fontSize: 19,
        fontWeight: 'bold',
        marginBottom: 7
    },
    tutorial: {
        fontSize: 16.5,
        color: '#333'
    }
})

export default Tutorial;