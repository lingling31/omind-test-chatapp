import React from 'react';
import { StyleSheet, Dimensions, TouchableOpacity, Image } from 'react-native';
import { Plus } from '../../../config/images';

const { width, height } = Dimensions.get('window')

const FloatButton = ({ onPress }) => {
    return (
        <TouchableOpacity
            style={styles.button}
            onPress={onPress}
        >
            <Image source={Plus} style={styles.add} />
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    button: {
        height: 55,
        width: 55,
        borderRadius: 55,
        backgroundColor: '#F22B5F',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        bottom: height * 0.125,
        right: width * 0.05,
        elevation: 3
    },
    add: {
        height: 21,
        resizeMode: 'contain',
        tintColor: '#fff'
    }
})

export default FloatButton;