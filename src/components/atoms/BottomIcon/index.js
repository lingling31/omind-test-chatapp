import React from 'react';
import { View, StyleSheet, Image, TouchableOpacity } from 'react-native';

const BottomICon = ({ item }) => {
    const { src, onPress, isActive } = item
    return (
        <View style={[styles.container, { marginTop: isActive ? -50 : 0 }]}>
            <TouchableOpacity
                style={isActive ? styles.buttonActive : styles.button}
                onPress={onPress}
            >
                <Image source={src} style={[styles.image, { tintColor: isActive ? '#fff' : '#999' }]} />
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    button: {
        height: 55,
        width: 55,
        borderRadius: 55,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonActive: {
        height: 55,
        width: 55,
        borderRadius: 55,
        backgroundColor: '#F22B5F',
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {
        height: 25,
        resizeMode: 'contain'
    }
})

export default BottomICon;