import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { DoubleCheck, Picture } from '../../../config/images';

const FieldChat = ({ item }) => {
    const { name, time, chat, unRead, typing } = item
    const hasImg = chat.match(/jpg/g)
    return (
        <TouchableOpacity style={styles.container}>
            <View style={styles.row}>
                <Text style={styles.name}>{name}</Text>
                <Text style={styles.time}>{time}</Text>
            </View>
            <View style={styles.row}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    {unRead === '' && !typing && <Image source={DoubleCheck} style={styles.check} />}
                    {hasImg && <Image source={Picture} style={styles.img} />}
                    <Text style={typing ? styles.typing : unRead ? styles.unReadChat : styles.chat} numberOfLines={1}>{chat}</Text>
                </View>
                <Text style={styles.unRead}>{unRead}</Text>
            </View>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    container: {
        padding: 10,
        flex: 1,
        justifyContent: 'space-around'
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    name: {
        fontSize: 17,
        fontWeight: 'bold',
    },
    time: {
        fontSize: 16,
        color: '#999',
    },
    chat: {
        color: '#999',
        width: '90%',
        marginLeft: -3
    },
    unReadChat: {
        fontSize: 15,
        width: '90%',
        color: '#888',
        fontWeight: 'bold'
    },
    unRead: {
        fontSize: 13,
        backgroundColor: '#F22B5F',
        color: '#fff',
        borderRadius: 15
    },
    typing: {
        fontSize: 16,
        color: '#F22B5F',
    },
    check: {
        height: 16,
        resizeMode: 'contain',
        tintColor: '#F22B5F',
        marginLeft: -7
    },
    img: {
        height: 15,
        resizeMode: 'contain',
        tintColor: '#aaa',
        marginLeft: -10
    }
})

export default FieldChat;