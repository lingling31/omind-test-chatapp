import React from 'react';
import { Image, TouchableOpacity, StyleSheet, View } from 'react-native';
import { Plus } from '../../../config/images';

const ProfileImage = ({ src, style, hasNewStory }) => {
    return (
        <TouchableOpacity style={styles.container}>
            {src === '+' ?
                <View style={styles.plus}>
                    <Image source={Plus} style={styles.imageAdd} />
                </View> :
                <View style={[styles.newStory, { borderColor: hasNewStory ? '#F22B5F' : '#fff' }]}>
                    <Image source={src} style={[styles.image, style]} />
                </View>
            }
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    container: {
        alignItems: 'center'
    },
    imageAdd: {
        height: 21,
        resizeMode: 'contain',
        tintColor: '#fff'
    },
    image: {
        height: 62,
        width: 62,
        borderRadius: 31,
    },
    plus: {
        height: 65,
        width: 65,
        backgroundColor: '#F22B5F',
        borderRadius: 32.5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    plusText: {
        color: '#fff',
        fontSize: 45
    },
    newStory: {
        borderColor: '#F22B5F',
        borderWidth: 2,
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: 70,
        borderRadius: 35
    }
})

export default ProfileImage;