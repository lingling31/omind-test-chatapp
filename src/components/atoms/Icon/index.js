import React from 'react';
import { Image, TouchableOpacity, StyleSheet } from 'react-native'

const Icon = ({ item }) => {
    const { src, style, onPress } = item
    return (
        <TouchableOpacity onPress={onPress}>
            <Image source={src} style={[styles.icon, style]} />
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    icon: {
        height: 19,
        resizeMode: 'contain'
    }
})

export default Icon;