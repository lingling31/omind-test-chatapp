import React from 'react';
import { View, StyleSheet } from 'react-native';
import { TabChat } from '../../molecules';
import { ChatList } from '../../organisms';

const ChatBox = ({ tabData = [], chatData = [] }) => {
    return (
        <View style={styles.container}>
            <TabChat data={tabData} />
            <ChatList data={chatData} />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
    },
})

export default ChatBox;