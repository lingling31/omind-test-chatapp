import React from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import { Story } from '../../molecules';

const Storyboard = ({ data = [] }) => {
    const mergeData = [{ id: 0, name: 'Ceritaku', src: '+' }, ...data]
    return (
        <View style={styles.container}>
            <FlatList
                data={mergeData}
                renderItem={({ item }) => <Story key={item.id} data={item} />}
                keyExtractor={(item) => item.id}
                horizontal
                showsHorizontalScrollIndicator={false}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        paddingVertical: 5
    }
})

export default Storyboard;