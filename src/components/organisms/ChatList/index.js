import React from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import { FieldChat } from '../../molecules';

const ChatList = ({ data = [] }) => {
    return (
        <View style={styles.container}>
            <FlatList
                data={data}
                renderItem={({ item }) => <FieldChat item={item} />}
                keyExtractor={(item) => item.id}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
    },
})

export default ChatList;